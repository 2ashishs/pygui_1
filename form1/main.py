from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.properties import (ListProperty, ObjectProperty)


class MyInput(TextInput):
    pass


class MyButton(Button):
    background_color = ListProperty([0.5, 0.5, 0, 1])


class MyForm(Widget):
    pass


class Form1App(App):
    def build(self):
        return MyForm()


Form1App().run()
